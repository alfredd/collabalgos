package main

import (
	"fmt"
	"log"
	"testing"
)

func assertEquals(actual, expected string) {
	if actual != expected {
		log.Fatalln("Failed")
	}
}

func TestOTEditor_Transformation(t *testing.T) {
	ot := OTEditor{Data: "yabcd", Ops: []Op{
		{Data: "abcd", Index: 0, Type: LOCAL, Op: INSERT},
		{Data: "y", Index: 0, Type: LOCAL, Op: INSERT},
	},
	}
	fmt.Println("Test 1. remote insert 'x' at index 2")
	ot.AppendOperation(INSERT, "x", 2, REMOTE)
	assertEquals(ot.Data, "yabxcd")
	fmt.Println("Test 2. remote delete char at index 1")
	ot.AppendOperation(DELETE, "", 1, REMOTE)
	assertEquals(ot.Data, "ybxcd")
	fmt.Println("Test 3. insert 'f' at index 1")
	ot.AppendOperation(INSERT, "f", 1, LOCAL)
	assertEquals(ot.Data, "yfbxcd")
	fmt.Println("Test 4. remote delete char at index 3")
	ot.AppendOperation(DELETE, "", 3, REMOTE)
	assertEquals(ot.Data, "yfbxd")
}
